import 'jquery';
import 'slick-carousel';
import jQuery from "jquery";
window.$ = window.jQuery = jQuery;

jQuery(function($) {
  $('.js-slider').slick( {
    dots: false,
    autoplay: false,
    adaptiveHeight: false
  });
});


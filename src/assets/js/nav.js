const menuButton = document.querySelector('.js-menu-button');
const nav = document.querySelector('.js-nav');
const navUnderlay = document.querySelector('.js-nav-underlay');

menuButton.addEventListener('click', () => {
  nav.classList.toggle('is-active');
  menuButton.classList.toggle('is-active');
  navUnderlay.classList.toggle('is-active');
});

const header = document.querySelector('.js-header');

window.addEventListener('scroll', () => {
  if (window.pageYOffset > 300) {
    header.classList.add('is-scrolled');
  } else {
    header.classList.remove('is-scrolled');
  }
});


